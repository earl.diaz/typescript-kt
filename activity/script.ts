type employeeIds = Array<Number>;
let staff_ids: employeeIds = [202201, 202202, 202203];

interface Employee {
    id: number,
    name: string,
    position: string,
    address?: string
}

function checkEmployment(employee: Employee): string {
    if (typeof employee === null || typeof employee === undefined) {
        return "You are not an employee!";
    }
    else {
        if (staff_ids.includes(employee.id)) {
            return "Welcome back " + employee.name + "!";
        }
        return "Employee not found!";
    }
}

let manager: Employee = {
    id: 202202,
    name: "Conan O'Brien",
    position: "Host",
    address: "Rockerfeller Center, New York City, NY"
}

console.log(checkEmployment(manager));