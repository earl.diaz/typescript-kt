var staff_ids = [202201, 202202, 202203];
function checkEmployment(employee) {
    if (typeof employee === null || typeof employee === undefined) {
        return "You are not an employee!";
    }
    else {
        if (staff_ids.includes(employee.id)) {
            return "Welcome back " + employee.name + "!";
        }
        return "Employee not found!";
    }
}
var manager = {
    id: 202202,
    name: "Conan O'Brien",
    position: "Host",
    address: "Rockerfeller Center, New York City, NY"
};
console.log(checkEmployment(manager));
