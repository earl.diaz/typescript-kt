function hello() {
    console.log("Hello, TineScript");
}
hello();
// To create variable in TS, we also use the let keyword.
// However, unlike in Java, we don't have to indicate the type of data.
var myNumber = 1;
// In typescript, We cannot update a variable to a different type:
// myNumber = "Hello";
// console.log(myNumber);
//Annotation in TS
//Annotation in TS, allows us ti add and enforce type checking for our variables:
//let varName: annotation;
var myNum;
myNum = 25;
//myNum = true
var myString;
//myString = {}
var myBool;
//myBool = "true"
//What if we want to create a variable that takes any type?
//let sample = 1;
//sample = "";
var unknownVar; // this variable is now able to take any type:
unknownVar = "unknown is my favorite pokemon";
console.log(unknownVar);
unknownVar = 12;
// we can slo add annotations to our arrays to limit them to a certain data type:
var myNumArr;
myNumArr = [1, 2, 3];
// myNumArr = [1, "2", "3"]; // error because of the set annotation
// myNumArr.push("Hello"); // array methods will not also work when different data type
myNumArr.push(6);
console.log(myNumArr);
//We can still create an array of any type:
var otherArr = ["John", 31, true];
// We can also still use typeof from JS to check the data type of a variable's data.
console.log(typeof unknownVar);
console.log(typeof myNumArr);
// Annotations in Functions
// Annotations can also be used to ensure the data type of the parameter or the return value:
function greet(name) {
    return "Hello, " + name + ", Welcome to the other side.";
}
// greet(25);
console.log(greet("Tolits"));
function add(num1, num2) {
    return num1 + num2;
}
//We can aslo add annotaions for the return value of a function
function sayMyName(name) {
    return "You are" + " " + name + ".";
}
//sayMyName(25);
sayMyName("Beyonce");
//Generics
//Allow us to create components/functions that can work over a variaty of types rather than a single one;
function genericFunctions(param) {
    return param;
}
var result1 = genericFunctions("Hello");
var result2 = genericFunctions(25);
console.log(result1);
console.log(result2);
var numberArray = [1, 4, 3];
var notification;
var newUser = {
    name: "Prince Ali",
    age: 16
};
var user3 = {
    username: "noobie191",
    password: "!234",
    age: 15,
    email: "noobie1991@mail.com"
};
//type UserAlias = {} // cannot add functionalities
function getUser() {
    var user = {
        username: "mynameisjeff",
        age: 15,
        password: "jumpst22",
        email: "channing@mail.com"
    };
    return user;
}
console.log(getUser());
function getAnimalHabitat(animal) {
    return animal.name + " lives in " + animal.habitat;
}
console.log(getAnimalHabitat({
    name: "Lion",
    habitat: "Savannah"
}));
//array <T> with Type Alias
var zooAnimals = [
    {
        name: "Tiger",
        habitat: "Jungle"
    },
    {
        name: "Orangutan",
        habitat: "Jungle"
    },
    {
        name: "Python",
        habitat: "Jungle"
    }
];
